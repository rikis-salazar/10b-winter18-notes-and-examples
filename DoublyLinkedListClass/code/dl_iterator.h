#ifndef DL_ITERATOR_H
#define DL_ITERATOR_H

#include "dl_declarations.h"
#include "dl_node.h"         // Node<T>


// Iterator Interface
template <typename T>
class Iterator { 
  private:
    Node<T>* the_node;

  public:
    Iterator( Node<T>* = nullptr );
                             // "Real" iterators use instead:
    T get_value() const;     // - `operator*` to get the value they point to.
    bool is_null() const;    // - `operator bool` & `==` to check for nullptr

    void move_forward();     // - pre, and post increment `++`
    void move_backward();    // - pre, and post decrement `--`

  friend class DoublyLinkedList<T>;
};



// Member functions
template<typename T>
Iterator<T>::Iterator( Node<T>* n ) : the_node(n) { }


template<typename T>
T Iterator<T>::get_value() const {
    return the_node->data;
}


template<typename T>
bool Iterator<T>::is_null() const {
    return ( the_node == nullptr );
}


template<typename T>
void Iterator<T>::move_forward(){
    if ( the_node )
        the_node = the_node->next;
    return;
}
// Note:
//     Attempting to move past `last()` is a 'no op' (i.e., no operation is
//     actually performed).


template<typename T>
void Iterator<T>::move_backward(){
    // Are we past `last()`?
    if ( the_node == nullptr )
        return;    // Sorry, cannot go back in.

    // Note:
    //     We are certain the_node can be dereferenced (->) safely.
    //     The question is now if we can move past `first()`. Unfortunately,
    //     this implementation does not allow us to check this. So, use this
    //     class carefully. Always make sure the head of the list is properly
    //     set (e.g., head-> prev points to nullptr).
    if ( the_node->prev )
        the_node = the_node->prev;

    return;
}

#endif
