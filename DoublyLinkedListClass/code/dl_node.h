#ifndef DL_NODE_H
#define DL_NODE_H

#include "dl_declarations.h"

// Interface
template <typename T>
class Node { 
  private:
    T data;
    Node* prev;
    Node* next;

  public:
    Node( const T& , Node* = nullptr, Node* = nullptr );

  friend class Iterator<T>;
  friend class DoublyLinkedList<T>;
};



// Function definitions
template<typename T>
Node<T>::Node( const T& d, Node<T>* p, Node<T>* n )
  : data(d), prev(p), next(n) { }

#endif
