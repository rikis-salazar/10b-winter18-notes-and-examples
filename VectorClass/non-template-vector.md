---  
title: 'Our very own `Pic10b::vector` class'  
author: 'Ricardo Salazar'  
date: 'Winter 2018'  
header-includes:  
  - \usepackage{caption}  
  - \usepackage[normalem]{ulem}  
  - \usepackage{letltxmacro}  
---  

>   **Disclaimer:** The contents of this document are mostly taken from a
>   handout I wrote during the Summer of 2017. It reflects the topics we
>   covered, as well as the decisions me wade _at that point in time_. As I
>   "rewrite" this handout, I'll try to update it to better represent the
>   directions we followed during lecture. If you encounter inconsistencies
>   please let me know so that I can correct them as soon as possible.


## The goal

During lecture we explained with an activity that letting the compiler provide
_the big 4_ for a _vector-like_ object is not a good idea. This is because
_vector-like_ objects keep track of data via memory addresses that are stored
in pointer variables. Issues might arise, for example, because data associated
to different objects might be of different sizes; or because copying the memory
address stored in a pointer might lead to data corruption. Our goal with this
handout is to understand how to properly code _the big 4_ for our very own
`vector` class.

### Wait! Isn't `vector` a reserved word?

The answer is: _It depends!_ 

If we include the `<vector>` library, **and** if we carelessly write an alias,
like `using namespace std;` or `using std::vector;`, then the answer is _yes_.
On the other hand, if we stay clear of the `std::vector` class, or if we use
fully qualified names like `std::vector<int>`, then the answer is _no_.


### The `Pic10b` namespace

In order to avoid possible name collisions with the `vector` name class, we
will _enclose it_ inside its own namespace: `Pic10b`. Thus, the fully qualified
name will be `Pic10b::vector`. For now, this namespace will consist of just the
_non-template_ vector class, and in the near future you will add at least one
more class to it (when you work on your next assignment). 

As it can be expected from the previous statement, one of the good things about
namespaces is that individual components can be defined in different locations.
For example, if we wanted to add the `Cosa` class to the `Pic10b` namespace we
could write a library file that looked like this

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
#ifndef THE_NAME_OF_THE_LIBRARY_FILE_IN_UPPERCASE_LETTERS_H
#define THE_NAME_OF_THE_LIBRARY_FILE_IN_UPPERCASE_LETTERS_H

// 1st part of `Pic10b`
namespace Pic10b{
    // `Pic10b::vector` class
    class vector{
      private:
        ...

      public:
        ...
    };

    // `Pic10b::some_fun` function
    void some_fun(...){
        ...
    }
}

// `some_other_fun` function (not part of `Pic10b`)
int some_other_fun(...){
    ...
}

// 2nd part of `Pic10b`
namespace Pic10b{
    // `Pic10b::Cosa` class
    class Cosa{
      private:
        ...

      public:
        ...
    };

    // `Pic10b::yet_one_more_fun` function
    double yet_one_more_fun(...){
        ...
    }
}

// `arent_you_tired_of_having_fun` function (not part of `Pic10b`)
int arent_you_tired_of_having_fun(...){
    ...
}

#endif
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 


### What is a `vector`, anyway?

During lecture we agreed that the main characteristic of a vector is that _all
of its elements are located next to each other_. Whereas the same can be said
about arrays, unlike arrays

*   vectors _know_ how big/small they are, and
*   they can be _resized_.

Since all elements are to be next to each other, in order to find the $k$-th
entry of a vector, it suffices to know:

i.  where the first element is located, and
i.  how _big_ these elements are.

With this information, we can locate the $k$-th entry via the formula
$$\text{location of } k \text{-th entry} = 
      \text{location of } 1 \text{-st entry} +
      \big( (k-1) \times \text{size of elements} \big),
      \quad k = 1,2,\dots$$

Here by _location_ we mean the memory address of an element, which  in most
cases is a hexadecimal number of the form `bxhhhhhhhh`, where `b` is `0`, or
`1`, and `h` is either a digit (`0--9`) or a letter (`a--f`) representing the
range `[0,f] = [0,15]`. The _size of elements_ that appears in the equation
above is a quantity that depends on the type. For example, in most _single
core_ computer architectures, the size of an `int` is 4 bytes, whereas the size
of a `double` is 16 bytes. 

Does this mean that we have to code this formula if we want to home-cook our
own `vector` class? Luckily for us the answer is: no! This is because
**pointer** variables not only allow us to store locations (memory addresses),
but they also _know_ how big a type is. The size of a type is always included
when using **pointer arithmetic**. 

For example if `ptr` is an `int` pointer, and if it stores memory address
`0xb1e2e3f4` then 
\begin{center}
    \texttt{int* ptr2 = ptr + 1;} stores
    \texttt{0xb1e2e3f8 = 0xb1e2e3f4 + 1 * 4}; 
\end{center}
similarly,
\begin{center}
    \texttt{int* ptr3 = ptr + 2;} stores
    \texttt{0xb1e2e3fc = 0xb1e2e3f4 + 2 * 4}.
\end{center}

On the other hand, if `ptr` holds `0xbeef430c` (memory address of a `double`),
then
\begin{center}
    \texttt{double* ptr2 = ptr + 2;} stores
    \texttt{0xbeef432c = 0xbeef430c + 2 * 16};
\end{center}
and on a similar note,
\begin{center}
    \texttt{double* ptr3 = ptr + 10;} stores
    \texttt{0xbeef43ac = 0xbeef430c + 10 * 16}.
\end{center}

One more thing to notice is that pointer arithmetic, together with the fact
that the first element of a vector has index zero, make for a simpler formula
to locate the (_k_+1)-th  entry (_i.e._ the one with index _k_). Namely, if
`ptr = &v[0]` holds the location of the first entry in a vector `v`, then
\begin{center} \texttt{\&v[k] = ptr + k}, \end{center} or, in terms of the
actual values (instead of their locations), we have \begin{center} \texttt{v[k]
    = *(ptr + k)}.  \end{center}

Let us pause for a moment to recapitulate, and to figure out what member fields
to include in our vector class. So far, we know that a vector:

*   _knows how big it is_; we should have a record of how many entries have been
    added to it (see `the_size` below).
*   _stores its entries next to each other_; we should have a record of where the
    first entry (0 index) is located, to be able to locate all other entries
    (see `the_data` below).
*   _stores its entries next to each other_[^again]; we should make sure that a
    big enough piece of memory has been reserved for it. We should also keep a
    record of how big this piece of memory is (see `the_capacity` below).

[^again]: Did I already say that?

This should be enough for us to get started. Assuming we want to code a vector
storing `double` elements, this is how our interface should look like

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
class vector{
  private:
    double* the_data;
    int the_size;
    int the_capacity;

  public:
    ...
};
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 


## The big 4

This is one of the cases where we need to provide custom versions of these
functions in order to avoid data corruption and/or program crashes. The main
issue here is how to properly request/release **dynamic memory**, that is,
memory allocated on _the heap_ instead of _the stack_[^stack].

[^stack]: Recall that _stack_ memory is self managed; whereas _heap_ memory is
not.


### The default constructor

This function needs to:

i.  initialize all fields; and
i.  decide whether or not an initial request for memory will be made.

For our `Pic10b::vector` class we will place an initial request for ~~2018~~ 10
`double` variables.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
namespace Pic10b{
    class vector{
        ...
    };

    ...

    // Default constructor
    vector::vector() {
        const int INITIAL_CAPACITY = 10;

        the_size = 0;
        the_capacity = INITIAL_CAPACITY;

        the_data = new double[the_capacity];
    }
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

>   **Note:**
>
>   We should point out that this is not the most efficient way to code it, but
>   for didactic purposes it is good enough. You are encouraged to write a
>   better version yourself, and/or compare it to the one that we coded during
>   lecture.


### The destructor

The purpose of this function is to _perform clean up dutty_. That is, it needs
to make sure that heap memory is released. In our case, we only need to deal 
with the memory pointed at by `the_data`. Since this memory was requested via
a call to the bracket version of `new`, we need to make an appropriate call to
the bracket version of `delete`.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
namespace Pic10b{
    ...

    vector::~vector(){
        delete[] the_data;
    }
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

>   **Important:**
>
>   Please note that `delete` is a rather unfortunate name for the operator that
>   releases memory. The data stored in the heap **is not** deleted (_i.e.,_ it
>   does not disappear), it actually **stays put**. Moreover, `the_data` still
>   holds the same memory address (_i.e.,_ the one used to access the entries of
>   the vector). What happens instead, is that the operating system (_e.g._
>   Windows, Android, Mac OS, etc.) receives notice that this _"deleted"_ memory
>   is available for programs to use, including perhaps the very same program
>   that deleted this memory in the first place.
>
>   Needless to say that even _"deleted"_ memory can still lead to data
>   corruption and/or crashes.  For example, if the location stored in
>   `the_data` is also stored in a different pointer[^sharing]; or if a pointer
>   _pointing at_ a deleted location attempts to access (_i.e.,_ dereference)
>   it; or even if there is an attempt to _release_ memory that has already
>   been deleted[^double-deletion].

[^sharing]: This is known as _pointer sharing_.
[^double-deletion]: This is known as _double deletion_.


### The copy constructor

The key here is to avoid a _complete shallow copy_ of the object that is passed
as a parameter to the function. Instead, we should 

i.  request memory in the heap (to store a copy of the entries of the passed
    vector) and store this location in `the_data`; and
i.  update the fields `the_size` and `the_capacity`  according to the current
    state of the vector.  This typically involves performing a _partial shallow
    copy_ of the fields of the parameter


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
namespace Pic10b{
    ...

    vector::vector( const vector& source ){
        the_data = new double[source.the_capacity];
        for ( int i = 0 ; i < source.the_size ; ++i )
            *(the_data + i) = *(source.the_data + i);

        the_capacity = source.the_capacity;
        the_size = source.the_size;
    }
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

>   **Note:** 
>
>   As is the case with the copy constructor, the implementation above is not
>   the most efficient. Can you improve it? If so, how?


### The assignment operator

The function is similar to the copy constructor in the sense that a deep copy
of the parameter will be made. Also in most cases involving variable-sized data,
not only heap memory from the implicit parameter needs to be released, but also
new memory needs to be reserved for the implicit object. In other words, this
operator is a combination of a constructor and a destructor. As such, when
implementing it we can either repeat code, or we can reuse functions. This
latter approach will be studied in Pic10C; it is known as the _copy and swap_
idiom.

In our case, we'll content ourselves with a 4 step process:

i.  Check for self-assignment. _Why spend energy, if we dont have to?_
i.  Release memory from the implicit object[^repeat-destruct].
i.  Reserve heap memory for the implicit object and perform a copy of the
    parameter[^repeat-construct].
i.  Return the implicit object.

[^repeat-destruct]: This is what a typical destructor does.
[^repeat-construct]: This is what a typical [copy] constructor does.

Here is one possible way to implement the assignment operator:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
namespace Pic10b{
    ...

    vector& vector::operator=( const vector& rhs ){
        if ( this != &rhs ){
            delete[] the_data;

            the_size = rhs.the_size;
            the_capacity = rhs.the_capacity;

            the_data = new double[the_capacity];
            for ( int i = 0 ; i < rhs.the_size ; ++i )
                *(the_data + i) = *(rhs.the_data + i);
        }
        return *this;
    }
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 


## Other member functions

We are certainly not done yet! We are missing key functionality like _getters_
and _setters_, as well as functions like `push_back`, `insert`, etc. These
missing functions can be divided in two groups, namely, those that might
potentially change the size of the vector; and those that leave the size of the
vector unchanged. Let us analyze at least one function from each of these two
groups.


### Size-altering functions

Here we will only study the `push_back(...)` member function. It falls into the
category of _size altering functions_ because once `the_size` reaches
`the_capacity`, there is no more room to store elements in the vector. As a
result, new heap memory needs to be requested and the elements of the vector
need to be transferred over to this new location; once this is done, the old
heap memory needs to be released.[^yet-again]

[^yet-again]: I do not know about you, but this is becoming a little repetitive
to a little repetitive to me me.

While this seems simple enough, we have to be careful. If every time the vector
reaches capacity (_i.e.,_ `the_size` equals `the_capacity`) we request _too
little extra memory_, then we could potentially end up performing several 
extra transfers of the data stored in the vector. On the other hand, if _too
much memory_ is requested, then we might prevent other objects from storing data
in the heap, eventually causing our program to crash.

For our implementation, we will get a clue from the way `std::vector` objects
handle resizing requests: every time capacity is reached, the function
`push_back(...)` **_doubles_** the current capacity of the object.

Summarizing, when the function `push_back(...)` is called, we check whether or
not capacity has been reached:

*   if this is not the case (_i.e.,_ `the_size < the_capacity`), we store the
    new value at index `the_size`;
*   else, we request double the current amount of memory, then proceed as if we
    were coding the assignment operator. Finally, we store the new value at
    index `the_size`.

Extracting the common code in both decision branches above leads to

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
namespace Pic10b{
    ...

    void vector::push_back( double val ){
        if ( the_size == the_capacity ){
            double* old_data_location = the_data;

            the_data = new double[2 * the_capacity];
            for ( int i = 0 ; i < the_size ; ++i )
                *(the_data + i) = *(old_data_location +i);

            delete[] old_data_location;
        }

        *(the_data + the_size) = val;
    }

}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 


### Non size-altering functions

Most of these functions are pretty straightforward. These include: `size()`,
`capacity()`, `empty()`, `front()`, `back()`, etc. The only one worth a special
mention here is the square brackets operator: `operator[](...)`.  This is the
typical _getter that acts like setter_ type of function that we have encountered
before. As such, we should expect to code two mostly similar versions: the
`const` version, and the non-`const` version.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
namespace Pic10b{
    ...

    double vector::operator[]( int index ) const {
        return *(the_data + index);
    }

    double& vector::operator[]( int index ) {
        return *(the_data + index);
    }
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 


## The `Pic10b::vector` (revisited)
 
Here is a slightly modified version of the class. Some of the changes include:

*   More efficient versions of constructors.

*   Addition of a `static` constant to represent the initial capacity of a
    vector.

*   Addition of a `private` member function `reserve(..)` that handles the
    requests of memory when capacity is reached. It makes sure that, regardless
    of the size requested, the new capacity is _at least double the size_ of the
    old capacity.

*   Use of _array-like_ syntax for pointers via the formula: `*(pointer +
    int_offset) = pointer[int_offset]`.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
namespace Pic10b{
    class vector{
      private:
        double* the_data;
        size_t the_size;
        size_t the_capacity;
        static const int INIT_CAP = 10;

      public:
        vector();
        vector( const vector& );
        vector& operator=( const vector& );
        ~vector();

        double& operator[]( size_t index );
        double operator[]( size_t index ) const;
        bool empty() const;
        size_t size() const;
        size_t capacity() const;
        ...

      private:
        void reserve( size_t new_capacity );
    };

    // Default construstor
    vector::vector()
      : the_data(nullptr), the_size(0), the_capacity(INIT_CAP) {
        the_data = new double[the_capacity];
    }

    // Copy constructor
    vector::vector( const vector& source )
      : the_data(nullptr), the_size(source.the_size),
        the_capacity(source.the_capacity) {
        the_data = new double[the_capacity];
        for ( int i = 0 ; i < the_size ; ++i ){
            the_data[i] = source.the_data[i];
        }
    }

    // Assignment operator
    vector& vector::operator=( const vector& rhs ) {
        if ( this != &rhs ) {
            delete[] the_data;

            the_size = rhs.the_size;
            the_capacity = rhs.the_capacity;

            the_data = new double[rhs.the_capacity];
            for ( int i = 0 ; i < the_size ; ++i )
                the_data[i] = rhs.the_data[i];
        }
        return *this;
    }

    // Destructor
    vector::~vector(){
        delete[] the_data;
    }

    bool vector::empty() const {
        return the_size == 0;
    }

    size_t vector::size() const {
        return the_size;
    }

    size_t vector::capacity() const {
        return the_capacity;
    }

    double& vector::operator[]( size_t index ){
        return the_data[index];
    }

    double vector::operator[]( size_t index ) const {
        return the_data[index];
    }

    void vector::reserve( size_t new_capacity ){
        if ( new_capacity > the_capacity ) {
            if ( new_capacity <= 2 * the_capacity )
                new_capacity = 2 * the_capacity;

            double* old_location = the_data;

            the_data = new double[new_capacity];
            the_capacity = new_capacity;

            for ( size_t i = 0 ; i < the_size ; ++i )
                the_data[i] = old_location[i];

            delete old_location;
        }
    }

}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
