# PIC 10B: Intermediate Programming (winter 2018)

This is the "unofficial" class website. Here you will find handouts, slides, and
code related to the concepts we discuss during lecture.

## _Home-made_ classes

 *  [The `Fraction` class][fraction-class]

     -  Lecture 1: [summary (slides)][lec1-pdf], [code][lec1-code].
     -  Lecture 2: [summary (slides)][lec2-pdf], [code][lec2-code],
        [code (clean version)][lec2-code-clean].
     -  Lecture 3: [summary (slides)][lec3-pdf], [code][lec3-code],
        [code (clean version)][lec3-code-clean].
     -  Lecture 4: [summary (slides)][lec4-pdf], [code][lec4-code],
        [code (clean version)][lec4-code-clean].
     -  Lecture 5: [summary (slides)][lec5-pdf], [code][lec5-code],
        [code (clean version)][lec5-code-clean].
     -  Lecture 6: [summary (slides)][lec6-pdf], [code][lec6-code],
        [code (clean version)][lec6-code-clean].

     Additionally, take a look at [this mostly equivalent implementation][tfl]
     of the `Fraction` class. It corresponds to the approach I followed during
     the 2017 summer session. Notice that unlike our class, arithmetic operators
     (_e.g.,_ `+`, `-`, etc.) are implemented in terms of the member ones.

 *  [The `Cosa` class][cosa-class]

     -  Lecture 1: [summary (slides)][cosa-lec1-pdf], [code][cosa-lec1-code],
        [code (clean version)][cosa-lec1-code-clean].

 *  [The `static` keyword][misc-examples]

     -  [`static.cpp`][static-code]: example that illustrates the
        initialization and use of different types of static members of a class
        (_e.g.,_ primitive types, objects, `const` fields/functions, etc.).

 *  [The non-template `Pic10b::vector` class][non-templ-vector]

     -  [A vector of `double`s][vector-handout]: a handout that explains the
        ideas and techniques behind the implementation of a vector class.

     Additionally, [take a look at this code][non-templ-cpp]; or better yet,
     try to implement the _well-known_ vector functions that were not discussed
     in detail in the handout. _E.g.:_ `size()`, `empty()`, `pop_back()`,
     `push_front()`, etc.

 *  [The template `DoublyLinkedList` class][DLL]

     -  [A _home-made_ list][list-handout]: a handout that explains the
        ideas and techniques you might need in order to implement a stack class.

     Additionally, take a look at the similarly named `DoublyLinkedList` class,
     whose code can be found at the [Old code [deprecated]][old-DLL] section at
     the CCLE class website. See if you can spot _improvements_ and/or _bugs_.

[fraction-class]: FractionClass/
[lec1-pdf]: FractionClass/Lecture1.md.pdf
[lec1-code]: FractionClass/lec1_code/fraction.cpp
[lec2-pdf]: FractionClass/Lecture2.md.pdf
[lec2-code]: FractionClass/lec2_code/fraction.cpp
[lec2-code-clean]: FractionClass/lec2_code/clean-fraction.cpp
[lec3-pdf]: FractionClass/Lecture3.md.pdf
[lec3-code]: FractionClass/lec3_code/fraction.cpp
[lec3-code-clean]: FractionClass/lec3_code/clean-fraction.cpp
[lec4-code]: FractionClass/lec4_code/fraction.cpp
[lec4-code-clean]: FractionClass/lec4_code/clean-fraction.cpp
[lec4-pdf]: FractionClass/Lecture4.md.pdf
[lec5-code]: FractionClass/lec5_code/fraction.cpp
[lec5-code-clean]: FractionClass/lec5_code/clean-fraction.cpp
[lec5-pdf]: FractionClass/Lecture5.md.pdf
[lec6-code]: FractionClass/lec6_code/fraction.cpp
[lec6-code-clean]: FractionClass/lec6_code/clean-fraction.cpp
[lec6-pdf]: FractionClass/Lecture6.md.pdf
[tfl]: FractionClass/three-file-layout/

[cosa-class]: CosaClass/
[cosa-lec1-code]: CosaClass/lec1_code/cosa.cpp
[cosa-lec1-code-clean]: CosaClass/lec1_code/clean-cosa.cpp
[cosa-lec1-pdf]: CosaClass/Lecture1.md.pdf

[misc-examples]: MiscExamples/
[static-code]: MiscExamples/static.cpp

[non-templ-vector]: VectorClass/
[vector-handout]: VectorClass/non-template-vector.pdf
[non-templ-cpp]: VectorClass/non-template-vector.cpp

[DLL]: DoublyLinkedListClass/
[list-handout]: DoublyLinkedListClass/doubly-linked-list.pdf
[old-DLL]: https://ccle.ucla.edu/course/view/18W-COMPTNG10B-3?section=6


## Other topics

In addition to our _home-made_ classes, we have also discussed other topics
where coding has not been the main focus. These topics include [but are not
limited to]:

*   Searching algorithms (linear and binary search).
*   [Big Oh notation.][big-oh]
*   [Recursion.][recursion]
*   [Complexity (costing) of recursive algorithms.][cost]
*   [Container adapters.][adapters]
*   [Binary search threes.][trees]
*   [Heaps and priority queues.][heap]
*   [Sorting algorithms.][sort]

[big-oh]: https://docs.google.com/presentation/d/11UZY6dvgNY1S1vXEZZZBsFUa9kz7_Udd3xbRRxy71wM/pub?start=false&loop=false&delayms=3000&slide=id.p
[recursion]: https://docs.google.com/presentation/d/1aRnGfG3DofjW9EJtD0Gm7igp7204syI9An74qTy4NHQ/pub?start=false&loop=false&delayms=3000&slide=id.p
[cost]: https://docs.google.com/presentation/d/1NQ74tJYOJi_fuidBUTKt61dCPcOJ2tdwfCxvJ2oepY8/pub?start=false&loop=false&delayms=3000&slide=id.p
[adapters]: https://docs.google.com/presentation/d/1GHeLx0oPeyvMo1gMV0NgQkXhl-o-3IzXMB_usLBZ7aM/pub?start=false&loop=false&delayms=3000&slide=id.p
[trees]: https://docs.google.com/presentation/d/1ZeiO7AQ9Mzde0Q4RIz8sWjehewgFUfMKziBb1ZJ-kBw/pub?start=false&loop=false&delayms=3000&slide=id.p
[heap]: https://docs.google.com/presentation/d/1z1_I-PF_K_5BBUktxrvMip9GiDte3QkX8vDE-dG_9vw/pub?start=false&loop=false&delayms=3000&slide=id.p
[sort]: https://docs.google.com/presentation/d/1svC0MrKbCOvO5fWJDu6p5QRYWeSSKkP4Ipaq42n61Xk/pub?start=false&loop=false&delayms=3000&slide=id.p

The [Old lessons [deprecated]][old-lessons] section of the CCLE class website
contains material that you might want to review at some point. Just keep in mind
that these presentations correspond to the way I used to teach this class a
little over a year ago.

In addition, you might also find some of the [Old code [deprecated]][old-code]
useful. Check it out whenever you have a chance.

[old-lessons]: https://ccle.ucla.edu/course/view/18W-COMPTNG10B-3?section=5
[old-code]: https://ccle.ucla.edu/course/view/18W-COMPTNG10B-3?section=6


## What about the textbook?

As you can probably tell, we are not following the textbook in a particular
order. I'm sorry to tell you that things will get worse before they get
better... as we will continue to jump back and forth between different sections.
To help you navigate through this maze, below I will be listing the names of the
chapters/sections where you can find relevant information.

 *  _Chapter:_ Classes  

     -  _Section(s):_ Interfaces, Encapsulation, Member functions, Default
        constructors, Constructor with parameters, Comparing member functions
        with non-member functions.

 *  _Chapter:_ Streams

     -  _Section(s):_ Reading and writing text files, The inheritance hierarchy
        of stream classes (superficial reading is more than enough, as I will
        not talk about inheritance).

 *  _Chapter:_ Operator overloading

     -  _Section(s):_ Operator overloading, Case study: Fractional numbers,
        Overloading simple arithmetic operators, Overloading comparison
        operators, Overloading input and output, Overloading increment and
        decrement operators, Overloading the assignment operators, Overloading
        conversion operators.

 *  _Chapter:_ Vectors and arrays

     -  _Section(s):_ Removing and inserting vector elements.

 *  _Chapter:_ Pointers

     -  _Section(s):_ Pointers and memory allocation, Deallocating dynamic
        memory, Arrays and pointers.

 *  _Chapter:_ Memory management

     -  _Section(s):_ Categories of memory,  Common memory errors, Constructors
        (briefly mentions _the assignment operator_ as _"[one] similar to a
        constructor"_), Destructors, Case study: matrices.

 *  _Chapter:_ Templates

     -  _Section(s):_ Template functions, Template classes, Turning a class into
        a template.

 *  _Chapter:_ Sorting and Searching

     -  _Section(s):_ Searching (linear search), Binary search.

 *  _Chapter:_ Recursion

     -  _Section(s):_ Thinking recursively, The efficiency of recursion.

 *  _Chapter:_ Lists, queues and stacks

     -  _Section(s):_  Linked lists, implementing linked lists, the efficiency
        of list and vector operations, queues and stacks.

## Homework assignments

 1. **A grade calculator**

    - [Submission link (CCLE) & due date][hw1-CCLE-link].
    - [Project source files (Bitbucket repository)][hw1-src].
    - [Project files download (link to zip file)][hw1-dload].

 1. **A** `Time` **class**

    - [Submission link (CCLE) & due date][hw2-CCLE-link].
    - [Project source files (Bitbucket repository)][hw2-src].
    - [Project files download (link to zip file)][hw2-dload].

 1. **Complexity of the _linear search_ algorithm**

    - [Submission link (CCLE) & due date][hw3-CCLE-link].
    - [Project source files (Bitbucket repository)][hw3-src].
    - [Project files download (link to zip file)][hw3-dload].

 1. **The `Pic10b::vector<T>` class**

    - [Submission link (CCLE) & due date][hw4-CCLE-link].
    - [Project source files (Bitbucket repository)][hw4-src].
    - [Project files download (link to zip file)][hw4-dload].

 1. **A _dragon_ curve**

    - [Submission link (CCLE) & due date][hw5-CCLE-link].
    - [Project source files (Bitbucket repository)][hw5-src].
    - [Project files download (link to zip file)][hw5-dload].

 1. **The _Hanoi_ puzzle**

    - [Submission link (CCLE) & due date][hw6-CCLE-link].
    - [Project source files (Bitbucket repository)][hw6-src].
    - [Project files download (link to zip file)][hw6-dload].


[hw1-CCLE-link]: https://ccle.ucla.edu/mod/assign/view.php?id=1824326
[hw1-src]: https://bitbucket.org/rikis-salazar/10b-hw-review/src
[hw1-dload]: https://bitbucket.org/rikis-salazar/10b-hw-review/downloads/

[hw2-CCLE-link]: https://ccle.ucla.edu/mod/assign/view.php?id=1824325
[hw2-src]: https://bitbucket.org/rikis-salazar/10b-hw-time-class/src
[hw2-dload]: https://bitbucket.org/rikis-salazar/10b-hw-time-class/downloads/

[hw3-CCLE-link]: https://ccle.ucla.edu/mod/assign/view.php?id=1824324
[hw3-src]: https://bitbucket.org/rikis-salazar/10b-hw-linear-search/src
[hw3-dload]: https://bitbucket.org/rikis-salazar/10b-hw-linear-search/downloads/

[hw4-CCLE-link]: https://ccle.ucla.edu/mod/assign/view.php?id=1824323
[hw4-src]: https://bitbucket.org/rikis-salazar/10b-hw-pic10b-vector/src
[hw4-dload]: https://bitbucket.org/rikis-salazar/10b-hw-pic10b-vector/downloads/

[hw5-CCLE-link]: https://ccle.ucla.edu/mod/assign/view.php?id=1824322
[hw5-src]: https://bitbucket.org/rikis-salazar/10b-hw-dragon-curve/src
[hw5-dload]: https://bitbucket.org/rikis-salazar/10b-hw-dragon-curve/downloads/

[hw6-CCLE-link]: https://ccle.ucla.edu/mod/assign/view.php?id=1824321
[hw6-src]: https://bitbucket.org/rikis-salazar/10b-hw-hanoi-puzzle/src
[hw6-dload]: https://bitbucket.org/rikis-salazar/10b-hw-hanoi-puzzle/downloads/


### About `.pdf` files

Please keep in mind that in order to access slides and/or handouts you need to
select the `view raw` link, or the `raw` button (on the top right corner). This
will likely prompt your web browser to download the corresponding document.
