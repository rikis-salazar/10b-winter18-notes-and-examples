#include "pic10b_fraction.h"

// using statements are OK in implementation files. Their scope is local.
using std::ostream;


// Constructor(s)
Fraction::Fraction( int n, int d ) : numer(n), denom(d) {
    normalize();
}


// Getter-setter functions
//     If any of the functions that return by reference below is to be used as a
//     setter, then it must be followed by a call to `normalize()`. This is to
//     make sure we keep `Fraction` objects consistent.
int& Fraction::numerator() {
    return numer;
}
int& Fraction::denominator() {
    return denom;
}
//     These other two are safe without `normalize()` because they are `const`.
int Fraction::numerator() const {
    return numer;
}
int Fraction::denominator() const {
    return denom;
}


// Member unary minus
Fraction Fraction::operator-() const { 
    return Fraction( -numer, denom );   // `normalize()` is indirectly called.
}



/* 
 * NOTE: during lecture we followed the opposite approach. Namely, we coded
 * `operator+=` by reusing `operator+`. This reverse approach is also valid.
 * In this case, we'll write code for all member operators, and we will reuse
 * it in non-member ones.
 */

// Member operators that modify the implicit object
Fraction& Fraction::operator+=( const Fraction& rhs ){
    // Recall:    p/q + r/s = ( p*s + q*r ) / q*s;
    int p = numer;
    int q = denom;
    int r = rhs.numer;
    int s = rhs.denom;

    numer = p*s + q*r;
    denom = q*s;

    normalize();

    return *this;
}
Fraction& Fraction::operator-=( const Fraction& rhs ){
    return *this += -rhs;   // No need to `normalize()`. Why?
}
Fraction& Fraction::operator*=( const Fraction& rhs ){
    numer *= rhs.numer;
    denom *= rhs.denom;

    normalize();

    return *this;
}
Fraction& Fraction::operator/=( const Fraction& rhs ){
    // Recall:    (p/q) / (r/s) = (p/q) * (s/r) 
    return *this *= Fraction( rhs.denom, rhs.numer );  // `normalize()`?
}


// Increment/decrement
Fraction& Fraction::operator++(){
    return *this += 1;
}
Fraction& Fraction::operator--(){
    return *this -= 1;
}

Fraction Fraction::operator++( int unused ){
    Fraction clone( numer, denom );
    ++*this;
    return clone;
}
Fraction Fraction::operator--( int unused ){
    Fraction clone( numer, denom );
    --*this;
    return clone;
}

/* **************** end of member fun. definitions ***************** */

// Conversions
Fraction::operator double() const {
    return static_cast<double>( numer ) / denom;
}
// Just because you can, it does not mean you should...
//     Fraction::operator bool() const {
//         return numer;       // Implicitly converted to bool (and int).
//     }

/* ************** end of member conversion operators *************** */

// Non-member friend operator(s)
ostream& operator<<( ostream& out, const Fraction& f ){
    if ( f.numer < 0 )
        out << '(' << f.numer << ')';
    else
        out << f.numer;
    
    out << '/' << f.denom;   // <-- Requires friendship

    return out;
}

// Non-member friend function(s)
int compare( const Fraction& lhs, const Fraction& rhs ) {
    // Recall that
    //     p/q < r/s   <==>   p/q - r/s < 0.
    //
    // In addition, we have
    //     p/q - r/s = ( p*s - r*q ) / q*s.
    //
    // Assuming both denominators are positive (i.e., the Fraction objects are
    // _normalized_, then
    //     p/q - r/s < 0   <==>   p*s - r*q < 0
    //
    // Summarizing:
    //     p/q <  r/s   <==>   p*s - r*q < 0
    //
    // Similarly, the outcome of all comparison between p/q and r/s is
    // determined by the int value p*s - r*q.
    //     p/q >  r/s   <==>   p*s - r*q > 0
    //     p/q <= r/s   <==>   p*s - r*q <= 0
    //     p/q >= r/s   <==>   p*s - r*q >= 0
    //     p/q == r/s   <==>   p*s - r*q == 0
    //     p/q != r/s   <==>   p*s - r*q != 0
    //
    // Hence this expression becomes the perfect candidate for the return of
    // this helper function.

    int p = lhs.numer;
    int q = lhs.denom;
    int r = rhs.numer;
    int s = rhs.denom;
    return p*s - r*q;
}

/* **************** end of friend fun. definitions ***************** */
bool operator<( const Fraction& f , const Fraction& g ){
    return compare(f,g) < 0 ;
}
bool operator>( const Fraction& f , const Fraction& g ){
    return compare(f,g) > 0 ;
}
bool operator<=( const Fraction& f , const Fraction& g ){
    return compare(f,g) <= 0 ; 
}
bool operator>=( const Fraction& f , const Fraction& g ){
    return compare(f,g) >= 0 ;
}
bool operator==( const Fraction& f , const Fraction& g ){
    return compare(f,g) == 0 ;
}
bool operator!=( const Fraction& f , const Fraction& g ){
    return compare(f,g) != 0 ;
}

/* ***************** end of boolean operators ******************* */

// Non-member, non-friend operators that reuse members
// NOTE: 
//     In all of the functions below, `lhs` is a copy (value param). Therefore,
//     it does not matter that the member operators alter its value.
Fraction operator+( Fraction lhs, const Fraction& rhs ){
    return lhs += rhs ;
}

Fraction operator-( Fraction lhs, const Fraction& rhs ){
    return lhs -= rhs ;
}

Fraction operator*( Fraction lhs, const Fraction& rhs ){
    return lhs *= rhs ;
}

Fraction operator/( Fraction lhs, const Fraction& rhs ){
    return lhs /= rhs ;
}

/* ******************* end of helper functions ****************** */

void Fraction::normalize(){
    // Fun fact: the ternary operator 
    //     var = cond ? val1 : val2
    // is the same as
    //    if ( cond ) var = val1; else var = val2;
    int sign = ( numer * denom  >= 0 ) ? 1 : -1 ;

    // Extract values (ignore signs)
    numer = ( numer >= 0 ) ? numer : -numer ;
    denom = ( denom >= 0 ) ? denom : -denom ;

    // If numerator is not 0 find the gcd and reduce
    int c = 1;
    if ( numer > 0 )
	c = gcd();

    // Keep sign in numerator and reduce
    numer = sign * ( numer / c ) ; 
    denom /= c;

    return;
}

int Fraction::gcd() const {
    int n = numer;
    int m = denom;

    // Procedure based on Euclid's Greatest Common Divisor algorithm. See: 
    // http://en.wikipedia.org/wiki/Euclidean_algorithm
    // Apply only if both numbers are strictly positive
    while ( n != m ) {   
	if ( n < m )  
	    m = m - n;
	else 
	    n = n - m;
    }   
   
    return n;
}

/* ********************** end of definitions ******************** */
