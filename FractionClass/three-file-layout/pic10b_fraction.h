#ifndef PIC10B_FRACTION_H
#define PIC10B_FRACTION_H

#include <iostream>          // std::ostream

// Never use global using statements in a library!!!
//     using std::ostream;


class Fraction{
  private:
    int numer;
    int denom;

  public:
    // constructor(s)
    Fraction( int = 0, int = 1 );

    // Access to private fields
    int& numerator();
    int& denominator();
    int numerator() const;
    int denominator() const;

    // Member operators 
    Fraction operator-() const ;

    Fraction& operator+=( const Fraction& );
    Fraction& operator-=( const Fraction& );
    Fraction& operator*=( const Fraction& );
    Fraction& operator/=( const Fraction& );

    Fraction& operator++();
    Fraction& operator--();
    Fraction operator++( int );
    Fraction operator--( int );

    // Conversions
    operator double() const;
    // The conversion below is not worth the trouble as it converts to `int` as
    // well, leading to ambiguity.
    //     operator bool() const;

    // Helpers
    void normalize(); 
    int gcd() const;

    // Friends 
    friend std::ostream& operator<<( std::ostream&, const Fraction& );
    friend int compare( const Fraction&, const Fraction& );
};

// Non-member, non-friend operators
Fraction operator+( Fraction, const Fraction& );
Fraction operator-( Fraction, const Fraction& );
Fraction operator*( Fraction, const Fraction& );
Fraction operator/( Fraction, const Fraction& );

#endif
