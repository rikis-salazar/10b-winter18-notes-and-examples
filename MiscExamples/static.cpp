// Ricardo Salazar 
// PIC 10B (Winter 2017)
//
// A simple example of static member fields as well as static member functions.
//
//
// Recall that during lecture we posed the problem of how to count the number
// of objects from a given class. We argued you that one possible way to do it
// was via a `static` counter. In this example I use said counter not only to
// solve the problem of counting objects, but also to assign a unique object
// 'identifier' to all instances of a class.


#include <iostream>  // std::cout
#include <string>    // std::string


// Class declaration
class Thing{
  private:
    int field1;
    int field2;

    // static int COUNT = 0;           // <--- No can do! Only `const` fields 
    //                 ^^^^^           //      can be initialized here.
    
    static int COUNT;                  // Initialized outside of the class.
    static const int DEF_INC = 10;     // OK, because `const` & primitive type.

    int id;                            // The unique 'identifier'.

  public:
    // Constructor(s) 
    Thing( int = 0, int = 1 );  // Arbitrary default values: 0, 1.

    // Displays id of an object
    int show_id() const;

    // Reports number of created objects 
    static int get_count();
    // static int get_count() const;
    //                        ^^^^^    // static members cannot be 'const'.

    // Adds increment to COUNT
    static void increase_count_by( int inc );

    // Same as above, but uses DEFAULT value
    static void increase_count_by_default_value();

    // Lastly, a PUBLIC static field
    //     static const std::string CLASS_NAME = "Thing";
    // In class object initialization is not     ^^^^^^^
    // possible for objects, only for primitive
    // types (see above).
    static const std::string CLASS_NAME;
};



// Definition/initialization of static member(s)
int Thing::COUNT = 0;

// const int Thing::COUNT = 10; // <-- Already initialized in interface

// Non-primitive `const` members are initialized outside the class.
const std::string Thing::CLASS_NAME = "Thing";




// Constructor(s)
Thing::Thing( int f1, int f2 )
  : field1(f1), field2(f2), id(++COUNT) {}
 
// Alternatively:
//     Thing::Thing( int f1, int f2 ) {
//         COUNT++;
//         id = COUNT;
//         field1 = f1;
//         field2 = f2;
//     }


// Non static member functions
int Thing::show_id() const {
    return id;
}



// static member functions
int Thing::get_count() {     // <-- no `const` because static members  do not
    return COUNT;            //     "belong" to the implicit object (*this), 
}                            //     they "belong" to the class.

void Thing::increase_count_by( int inc ){
    COUNT += inc;
    return;
}

void Thing::increase_count_by_default_value(){
    COUNT += DEF_INC;
    return;
}



/**
    // Turns out that our counter doesn't really count all that well. See note
    // after `main()`.
    //
    void dummy_fun( Thing t ){
        return;
    }
**/



// The driver
int main(){
    using std::cout;

    Thing t1;
    Thing t2(2);
    Thing t3(3,4);

    // dummy_fun(t3);       // <-- See note below!

    cout << "This class creates objects of type: "; 
    cout << Thing::CLASS_NAME << '\n';

    cout << "So far we have constructed " 
         << Thing::get_count() << " objects\n";

    cout << "Their ID's are (in reverse order):\n"
         << t3.show_id() << '\t' << t2.show_id() << '\t'
         << t1.show_id() << '\n';

    
    // `static` member functions can be called via a particular object, as in
    //     t3.increase_count_by(),
    // or
    //     t1.get_count() ...
    t3.increase_count_by(2018);
    cout << "If we assume we have created 2018 more objects\n"
         << "then the count would be " << t1.get_count() << '\n';

    // ... but the preferred way to call static member functions is by their
    // "class name", as in 
    //     Thing::increase_count_by_default_value(),
    // or 
    //     Thing::get_count()
    Thing::increase_count_by_default_value();
    cout << "After 10 more hypothetical objects, the count \n"
         << "would be " << Thing::get_count() << '\n';

    return 0;
}


/**
 *  Note(s): Our counter breaks down when objects are copied. For example, if we
 *           defined the function
 *
 *           void dummy_fun( Thing t ){
 *               return;
 *           }
 *
 *           and at some point made a call to it (see `main()`), our counter
 *           should increase by one after the end of this function. This is
 *           because a local thing 't' would be created and destroyed within
 *           this function.
 *
 *           Our current implementation of the `Thing` class does not account
 *           for copies. We will fix this later when we study 'The Big 3'.
**/


 
/**
    *******************  OUTPUT  *******************

    This class creates objects of type: Thing
    So far we have constructed 3 objects
    Their ID's are (in reverse order):
    3        2        1
    If we assume we have created 2018 more objects
    then the count would be 2021
    After 10 more hypothetical objects, the count 
    would be 2031

    ************************************************
**/
